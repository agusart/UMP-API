<?php
require_once('./vendor/autoload.php');
require_once('./cors.php');


use Firebase\JWT\JWT;
use Dotenv\Dotenv;

// Load dotenv
$dotenv = Dotenv::createImmutable(__DIR__);
$dotenv->load();

// Atur jenis response
header('Content-Type: application/json');

// Cek method request
if ($_SERVER['REQUEST_METHOD'] !== 'GET') {
  http_response_code(405);
  exit();
}

$headers = getallheaders();

if (!isset($headers['Authorization'])) {
  http_response_code(401);
  exit();
}

list(, $token) = explode(' ', $headers['Authorization']);

try {
  JWT::decode($token, $_ENV['ACCESS_TOKEN_SECRET'], ['HS256']);
  
  $user = [
    'name' => 'John Doe',
    'email' => 'johndoe@example.com'
  ];

  echo json_encode($user);
} catch (Exception $e) {
  http_response_code(401);
  exit();
}
